#ifndef TANKS_TASKS_HPP_
#define TANKS_TASKS_HPP_

#include "scheduler_task.hpp"
#include "FreeRTOS.h"
#include "MaxElements.hpp"
#include "Servo.hpp"

#include "WiiProController.hpp"
#include "laser.hpp"
#include "user_defined_types.h"
#include "game_mesh_net.hpp"
#include "tank_motor.hpp"
#include "stdio.h"
#include "printf_lib.h"

class OnScreenDisplayTask : public scheduler_task
{
	private:
		MaxLayoutManager * Layout;
		Bar * UserHP;
		Bar * EnemyHP;
		Cursor * CrossHair;
		Score * ScoreUI;
		PowerUps * PowerUpsUI;

		Label * Title;
		Label * StartGameMessage;
		Label * WaitingForOpponent;
		Label * BeginMessage;
		Label * GameOverTitle;
		Label * YouLose;
		Label * YouWin;

		GameState_e Current_State;

		uint8_t grouping;

	public:
		OnScreenDisplayTask(uint8_t priority) :
			scheduler_task("On Screen Display Task", 4056, priority) { }
		bool init(void);
		bool run(void *p);
};

class CannonTask : public scheduler_task
{
	private:
		Servo * Cannon;
		uint32_t Hold_Counter;
		uint32_t Position;
	public:
		CannonTask(uint8_t priority) :
			scheduler_task("Cannon Task", 2048, priority) { }
		bool init(void);
		bool run(void *p);
};

class FireTask : public scheduler_task
{
	private:

	public:
		FireTask(uint8_t priority) :
			scheduler_task("Fire Task", 2048, priority) { }


		bool init(void);
		bool run(void *p);
};

class HitTask : public scheduler_task
{
	private:

	public:
		HitTask(uint8_t priority) :
			scheduler_task("Hit Task", 2048, priority) { }
		bool init(void);
		bool run(void *p);
};

class ControllerTask : public scheduler_task
{
	private:
		WiiProController* Controller;
		Game_Mesh_Net* myNet;
	public:
	    ControllerTask(uint8_t priority) :
	    	scheduler_task("Wii Pro Controller Task", 2048
	    		, priority) { }
	    bool init(void);
	    bool run(void *p);
};

class LaserTask : public scheduler_task
{
	private:
		laser * left;
		laser * right;

	public:
		LaserTask (uint8_t priority) : scheduler_task ("LaserTask", 2048, priority) {}
		bool init (void);
		bool run (void *p);
};

class EngineTask : public scheduler_task
{
	private:
		Master_To_Tank_t *MTT_Master;
		Master_To_Tank_t *MTT_Slave;
		Tank_To_Master_t *TTM_Master;
		Tank_To_Master_t *TTM_Slave;
		Game_Mesh_Net* Net;

	public:
		EngineTask (uint8_t priority) : scheduler_task ("EngineTask", 2048, priority) {}
		bool init (void);
		bool run (void *p);
};

class EngineTaskTest : public scheduler_task
{
	private:
		Tank_To_Master_t * Tank;
		Game_Mesh_Net* Net;
		Master_To_Tank_t * Master;

	public:
		EngineTaskTest (uint8_t priority) : scheduler_task ("EngineTaskTest", 2048, priority) {}
		bool init (void);
		bool run (void *p);
};

class MeshNetReceive : public scheduler_task
{
private:
	Game_Mesh_Net* Net;
	mesh_packet_t pkt;
	bool debugMode;
public:
	MeshNetReceive(uint8_t priority) : scheduler_task("Mesh Net Receive", 2048, priority), Net() {}
	bool init(void);
	bool run(void *p);
};

class MasterMeshNetReceive : public scheduler_task
{
private:
	Game_Mesh_Net* Net;
	mesh_packet_t pkt;
	bool debugMode;
public:
	MasterMeshNetReceive(uint8_t priority) : scheduler_task("Master Mesh Net Receive", 2048, priority) {}
	bool init(void);
	bool run(void *p);
};

class DriveSystem : public scheduler_task
{
private:
	const uint8_t ControllerMin = 0;
	const uint8_t ControllerMidPoint = 16;
	const uint8_t ControllerMax = 32;
	const uint8_t PwmMin = 0;
	const uint8_t PwmMax = 100;
	TankMotor Left;
	TankMotor Right;
	TankMotor::Motor_Direction_E LeftDir;
	TankMotor::Motor_Direction_E RightDir;
	uint8_t LeftSpeed;
	uint8_t RightSpeed;
	Controller_t* ctrl;
	SemaphoreHandle_t* xSemTerm;
	bool debugMode;
public:
	DriveSystem(uint8_t priority) : scheduler_task("Drive System", 2048, priority),
									Left(PWM::pwm1, PWM::pwm2),
									Right(PWM::pwm3, PWM::pwm4) {}
	bool init(void);
	bool run(void *p);
	// from Arduino library
	long map(long x, long in_min, long in_max, long out_min, long out_max)
	{
  		return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
	}
};


class DriveSystemTest : public scheduler_task
{
private:
	Controller_t* ctrl;
public:
	DriveSystemTest(uint8_t priority) : scheduler_task("Drive System Test", 2048, priority) {}
	bool init(void);
	bool run(void *p);
};
#endif /* TANK_TASKS_HPP_ */
