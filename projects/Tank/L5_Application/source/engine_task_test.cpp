#include "tank_tasks.hpp"
#include "utilities.h"
#include "printf_lib.h"
#include "io.hpp"
#include "game_mesh_net.hpp"

bool EngineTaskTest::init (void)
{
	Tank = (Tank_To_Master_t *)getSharedObject("Tank_To_Master");
	Net = (Game_Mesh_Net *)getSharedObject("Net");

	Tank->ready = 0;
	Tank->health = 5;
	Tank->hit = 0;
	return true;
}

bool EngineTaskTest::run (void *p)
{
	Master = (Master_To_Tank_t *)getSharedObject("Master_To_Tank");

	vTaskDelay (100);
	Net->sendTankToMaster(10);
	// if(Net->sendTankToMaster(10))
	// 	printf ("Tank# %i got a Master Ack\n", Net->NetID());
	// else
	// 	printf ("Tank# %i got a Master NACK!\n", Net->NetID());

	switch (Master->game_state)
	{
		case (MAIN_MENU):
			printf ("Main Menu \n");

			if (SW.getSwitch(1)) Tank->ready = 1;
			printf ("ready = %i", Tank->ready);
		break;

		case (WAITING):
			printf ("Waiting \n");
		break;

		case (FIGHTING):
			printf ("Fighting \n");

			if (SW.getSwitch(2)) Tank->health--;
			printf ("health = %i", Tank->health);
		break;

		case (GAME_OVER_LOSE):
			printf ("Get Rekt Nerd \n");
		break;

		case (GAME_OVER_WIN):
			printf ("You Have Some Skill \n");
		break;
	}
	return true;

}