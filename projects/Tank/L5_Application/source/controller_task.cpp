#include "tank_tasks.hpp"
#include "game_mesh_net.hpp"

#include "scheduler_task.hpp"
#include "FreeRTOS.h"

bool ControllerTask::init(void)
{
    myNet = (Game_Mesh_Net*)getSharedObject("Net");
	Controller = new WiiProController;
    Controller->Initialize();
    return true;
}

bool ControllerTask::run(void *p)
{
    // Update controller information every 100ms
    Controller_t* State = (Controller_t *)getSharedObject("Controller");
    *State = Controller->Read();
    State->packetID = Packet_Src_t::CONTROLLER_ID;
    myNet->sendControllerToTank(100);
    vTaskDelay(100);
    return true;
}