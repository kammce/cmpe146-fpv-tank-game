#include "tank_tasks.hpp"

#include "scheduler_task.hpp"
#include "FreeRTOS.h"

#define FIRE_TIME 500
#define RELOAD_TIME 3000

bool FireTask::init(void)
{
	PWM transIR(PWM::pwm6, 38000);
	return true;
}
bool FireTask::run(void *p)
{
	
	Master_To_Tank_t * Master = (Master_To_Tank_t *)getSharedObject("Master_To_Tank");
    Tank_To_Master_t * Tank = (Tank_To_Master_t *)getSharedObject("Tank_To_Master");

	#if DEBUG
		Master->game_state = FIGHTING;
	#endif

	if(Master->game_state == FIGHTING)
	{
		Controller_t * Controller = (Controller_t *)getSharedObject("Controller");
		#if DEBUG
			Controller->BZR = 1;
		#endif
		if(Controller->BZR)
		{	
			#if DEBUG
				printf ("PP");
			#endif
			//NVIC_DisableIRQ(EINT3_IRQn);
			Tank->hit = 0;
			//vTaskDelay(1);
			PWM transIR(PWM::pwm6, 38000);
			
			transIR.set(30);
			vTaskDelay(FIRE_TIME);
			
        	transIR.set(0);
        	//vTaskDelay(1);
        	//NVIC_EnableIRQ(EINT3_IRQn);
			#if DEBUG
				Controller->BZR = 0;
			#endif
			Tank->hit = 1;
        	vTaskDelay(RELOAD_TIME);

		}
	}
	vTaskDelay(100);
	return true;
}
//testing
