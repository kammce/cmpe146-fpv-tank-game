#include "tank_tasks.hpp"

bool LaserTask::init(void)
{
	left = new laser (2, 6);
	right = new laser (1, 29);
	return true;
}

bool LaserTask::run (void * p)
{
	Master_To_Tank_t * Master = (Master_To_Tank_t *)getSharedObject("Master_To_Tank");

	if (Master -> game_state == FIGHTING)
	{
		left -> activatePin();
		right -> activatePin();
	}
	else
	{
		left -> deactivatePin();
		right -> deactivatePin();
	}

	vTaskDelay(1000);

	return true;
}