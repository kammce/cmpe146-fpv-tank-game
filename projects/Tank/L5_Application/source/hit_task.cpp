#include "user_defined_types.h"
#include "utilities.h"
#include "printf_lib.h"
#include "LPC17xx.h"
#include "lpc_isr.h"
#include "tank_tasks.hpp"
#include "FreeRTOS.h"
#include "scheduler_task.hpp"



typedef void (*funcPtr) (void);

typedef enum
{
    NORTH = 1,
    EAST = 2,
    SOUTH = 3,
    WEST = 4,
} Side_t;

typedef enum
{
    shared_SensorQueueId,
}sharedHandleId_t;

QueueHandle_t hitQueue;
Side_t lastHit = SOUTH;
static funcPtr portArray[2][32][2] = {0};

void updateHealth(void)
{
    Tank_To_Master_t * Master = (Tank_To_Master_t *)scheduler_task::getSharedObject("Tank_To_Master");
    Master->health = Master->health - 1;
    //Master->hit = 1;
    #if DEBUG
        u0_dbg_printf("\nHealth Updated %d", Master->health);
    #endif

}

static void configEInt(uint8_t portNum, uint8_t pinNum, bool rising, funcPtr ptr)
{
    switch(portNum)
    {
        case(0):
        if (rising)
        LPC_GPIOINT -> IO0IntEnR |= (1 << pinNum);
        else
        LPC_GPIOINT -> IO0IntEnF |= (1 << pinNum);

        portArray [0][pinNum][rising] = ptr;
        break;
        case(2):
        if (rising)
        LPC_GPIOINT -> IO2IntEnR |= (1 << pinNum);
        else
        LPC_GPIOINT -> IO2IntEnF |= (1 << pinNum);

        portArray [1][pinNum][rising] = ptr;
        break;
        default:
        break;
    }
    NVIC_EnableIRQ(EINT3_IRQn);
}
static void EINT3Handler (void)
{
    int mask;
    for (int i = 0; i < 32; i++)
    {
        mask = (1 << i);
        if (LPC_GPIOINT->IO0IntStatR & mask)
        portArray[0][i][1]();
        if (LPC_GPIOINT->IO0IntStatF & mask)
        portArray[0][i][0]();
        if (LPC_GPIOINT->IO2IntStatR & mask)
        portArray[1][i][1]();
        if (LPC_GPIOINT->IO2IntStatF & mask)
        portArray[1][i][0]();
    }
    LPC_GPIOINT->IO0IntClr = -1;
    LPC_GPIOINT->IO2IntClr = -1;
}

void configPins(void)
{
    //IR sensors
    LPC_PINCON->PINSEL4 &= ~(3 << 2);
    LPC_GPIO2->FIODIR &= ~(1 << 1);
    LPC_PINCON->PINSEL1 &= ~(3 << 2);
    LPC_GPIO1->FIODIR &= ~(1 << 1);
    //LED Pins
    LPC_PINCON->PINSEL1 &= ~(3<<20); //P0.26
    LPC_GPIO0->FIODIR |= (1<<26); // North Red
    LPC_GPIO0->FIOCLR  = (1<<26);

    LPC_PINCON->PINSEL3 &= ~(3<<8); //P1.20
    LPC_GPIO1->FIODIR |= (1<<20); // North Green
    LPC_GPIO1->FIOSET  = (1<<20);

    LPC_PINCON->PINSEL3 &= ~(3<<30); //P1.31
    LPC_GPIO1->FIODIR |= (1<<31); // West Red
    LPC_GPIO1->FIOCLR  = (1<<31);

    LPC_PINCON->PINSEL4 &= ~(3<<8); //P2.4
    LPC_GPIO2->FIODIR |= (1<<4);  // West Green
    LPC_GPIO2->FIOSET  = (1<<4);

    LPC_PINCON->PINSEL3 &= ~(3<<24);  //P1.28
    LPC_GPIO1->FIODIR |= (1<<28); // South Red
    LPC_GPIO1->FIOSET  = (1<<28);

    LPC_PINCON->PINSEL3 &= ~(3<<28); //1.30
    LPC_GPIO1->FIODIR |= (1<<30); // South Green
    LPC_GPIO1->FIOCLR  = (1<<30);

    LPC_PINCON->PINSEL3 &= ~(3<<12); //P1.22
    LPC_GPIO1->FIODIR |= (1<<22); // East Red
    LPC_GPIO1->FIOCLR  = (1<<22);

    LPC_PINCON->PINSEL3 &= ~(3<<14); //P1.23
    LPC_GPIO1->FIODIR |= (1<<23); // East Green
    LPC_GPIO1->FIOSET  = (1<<23);
    //South starts red
}

void ledUpdate(Side_t side)
{
    LPC_GPIO1->FIOSET  = (1<<20);
    LPC_GPIO2->FIOSET  = (1<<4);
    LPC_GPIO1->FIOSET  = (1<<30);
    LPC_GPIO1->FIOSET  = (1<<23);

    LPC_GPIO1->FIOCLR  = (1<<22);
    LPC_GPIO1->FIOCLR  = (1<<28);
    LPC_GPIO1->FIOCLR  = (1<<31);
    LPC_GPIO0->FIOCLR  = (1<<26);

    switch(side) {
        case (WEST):
            LPC_GPIO1->FIOSET  = (1<<31);
            LPC_GPIO2->FIOCLR  = (1<<4);
            break;
        case (EAST):
            LPC_GPIO1->FIOSET  = (1<<22);
            LPC_GPIO1->FIOCLR  = (1<<23);
            break;
        case (SOUTH):
            LPC_GPIO1->FIOSET  = (1<<28);
            LPC_GPIO1->FIOCLR  = (1<<30);
            break;
        case (NORTH):
            LPC_GPIO0->FIOSET  = (1<<26);
            LPC_GPIO1->FIOCLR  = (1<<20);
            break;
        default:
            u0_dbg_printf("\nERROR: ledUpdate %d", side);
    }
}

void checkStatus(Side_t side)
{
    Tank_To_Master_t * Tank = (Tank_To_Master_t *)scheduler_task::getSharedObject("Tank_To_Master");
    if(side != lastHit && Tank->hit == 1 ) {
        #if DEBUG
            u0_dbg_printf("\nCheck Staus %d ", Tank->hit);
        #endif
        lastHit = side;
        updateHealth();
        ledUpdate(side);
    }
}



static void rec_isr_north(void)
{
    #if DEBUG
        u0_dbg_printf("\nIR Received Hit Fore");
    #endif
        Side_t side = NORTH;
        xQueueSendFromISR(hitQueue, &side, 0);
}
static void rec_isr_south(void)
{
    #if DEBUG
        u0_dbg_printf("\nIR Received Hit Aft");
    #endif
        Side_t side = SOUTH;
        xQueueSendFromISR(hitQueue, &side, 0);
}
static void rec_isr_east(void)
{
    #if DEBUG
        u0_dbg_printf("\nIR Received Hit Port");
    #endif
    Side_t side = EAST;
    xQueueSendFromISR(hitQueue, &side, 0);
}
static void rec_isr_west(void)
{
    #if DEBUG
        u0_dbg_printf("\nIR Received Hit Starboard");
    #endif
    Side_t side = WEST;
    xQueueSendFromISR(hitQueue, &side, 0);
}



bool HitTask::init(void)
{
    configPins();
    configEInt(0,0,0,&rec_isr_west);
    configEInt(0,29,0,&rec_isr_east);
    configEInt(0,1,0,&rec_isr_south);
    configEInt(0,30,0,&rec_isr_north);
    isr_register(EINT3_IRQn, &EINT3Handler);
    hitQueue = xQueueCreate(1, sizeof(Side_t));
    addSharedObject(shared_SensorQueueId, hitQueue);
    return true;
}
bool HitTask::run(void *p)
{
    Side_t side;
    #if DEBUG
       u0_dbg_printf("\nIR Queue Waiting");
    #endif
    xQueueReceive(hitQueue, &side, portMAX_DELAY);
    #if DEBUG
       u0_dbg_printf("\nIR Queue Recieved %d", side);
    #endif
    checkStatus(side);
    vTaskDelay(3000);
    xQueueReceive(hitQueue, &side, 0);
    return true;
}

//funcPtr HitTask::portArray[2][32][2] = {0};