#include "tank_tasks.hpp"
#include "stdio.h"
#include <cstring>

bool MeshNetReceive::init(void)
{
    #if DEBUG
        MeshNetReceive::debugMode = true;
    #else
        MeshNetReceive::debugMode = false;
    #endif
    return true;
}

bool MeshNetReceive::run(void *p)
{
    static int counter = 0;
    SemaphoreHandle_t   * xMutexSPI     = (SemaphoreHandle_t *)scheduler_task::getSharedObject("xMutexSPI");
    vTaskDelay(50);

    if (wireless_get_rx_pkt(&pkt, 25))
    {
        //printf ("from : %i %i \n", pkt.data[0], counter++);

        switch (pkt.data[0])
        {
            case (int)Packet_Src_t::MASTER_ID :
                memcpy(getSharedObject("Master_To_Tank"), pkt.data, sizeof(Master_To_Tank_t));
                //if (debugMode) printf ("gamestate: %i\n", (Master_To_Tank_t*) scheduler_task::getSharedObject("Master_To_Tank"));
                break;
            case (int)Packet_Src_t::TANK_ID :
                memcpy(getSharedObject("Tank_To_Master"), pkt.data, sizeof(Tank_To_Master_t));
                break;
            case (int)Packet_Src_t::CONTROLLER_ID :
                if (debugMode) printf(";");
                memcpy(getSharedObject("Controller"), pkt.data, sizeof(Controller_t));
                if (debugMode) printf("shared.ly: %u, shared.ry: %u\n",((Controller_t*)getSharedObject("Controller"))->LY, ((Controller_t*)getSharedObject("Controller"))->RY);
                break;
            default :
                if (debugMode) printf("Couldn't decode this packet...\n");
        }
        wireless_flush_rx();
    }
    else
    {
        if (debugMode) printf(".");
    }

    return true;
}