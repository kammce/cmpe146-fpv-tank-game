#include "tank_tasks.hpp"
#include "stdio.h"
#include "stdlib.h"
#include "printf_lib.h"

bool DriveSystem::init(void)
{
    #if DEBUG
        DriveSystem::debugMode = true;
    #else
        DriveSystem::debugMode = false;
    #endif

	LeftDir = TankMotor::Motor_Direction_E::Forward;
	RightDir = TankMotor::Motor_Direction_E::Forward;
	LeftSpeed = 0;
	RightSpeed = 0;
	Right.MotorDir(RightDir);
	Right.MotorSpeed(abs(RightSpeed));
	Left.MotorDir(LeftDir);
	Left.MotorSpeed(abs(LeftSpeed));	

	ctrl = (Controller_t*) getSharedObject("Controller");

	return true;
}

bool DriveSystem::run(void *p)
{
//    if (debugMode) printf("shared.ly: %u, shared.ry: %u\n", ctrl->LY, ctrl->RY);

	uint8_t ly = ctrl->LY >> 1;
	uint8_t ry = ctrl->RY;

//	if (debugMode) printf("ly: %u, ry: %u\n", ly, ry);

	if(ry < ControllerMidPoint)
	{
		RightDir = TankMotor::Motor_Direction_E::Backward;
		RightSpeed = map(ry, ControllerMidPoint, ControllerMin, PwmMin, PwmMax);
	}
	else if (ry > ControllerMidPoint)
	{
		RightDir = TankMotor::Motor_Direction_E::Forward;
		RightSpeed = map(ry, ControllerMidPoint, ControllerMax, PwmMin, PwmMax);
	}
	else //ry == ControllerMidPoint ==> Stop
	{
		RightDir = TankMotor::Motor_Direction_E::Forward;
		RightSpeed = 0;
	}

	if(ly < ControllerMidPoint)
	{
		LeftDir = TankMotor::Motor_Direction_E::Forward;
		LeftSpeed = map(ly, ControllerMidPoint, ControllerMin, PwmMin, PwmMax);
	}
	else if (ly > ControllerMidPoint)
	{
		LeftDir = TankMotor::Motor_Direction_E::Backward;
		LeftSpeed = map(ly, ControllerMidPoint, ControllerMax, PwmMin, PwmMax);
	}
	else //ly == ControllerMidPoint ==> Stop
	{
		LeftDir = TankMotor::Motor_Direction_E::Forward;
		LeftSpeed = 0;
	}

	Right.MotorDir(RightDir);
	Right.MotorSpeed(abs(RightSpeed));
	Left.MotorDir(LeftDir);
	Left.MotorSpeed(abs(LeftSpeed));

//	if (debugMode) printf("L: %i, %i. R: %i, %i\n", LeftDir, LeftSpeed, RightDir, RightSpeed);

    vTaskDelay(200);
	return true;
}