#include "tank_tasks.hpp"
#include "stdio.h"
#include "stdlib.h"
#include "printf_lib.h"
#include "io.hpp"

bool DriveSystemTest::init(void)
{
	ctrl = (Controller_t*) getSharedObject("Controller");
	ctrl->RY = 16;
	ctrl->LY = 32;
	return true;
}

bool DriveSystemTest::run(void *p)
{
	ctrl = (Controller_t*) getSharedObject("Controller");
	if (SW.getSwitch(1))
	{
		++(ctrl->RY);
	}

	if (SW.getSwitch(2))
	{
		--(ctrl->RY);
	}

	if (SW.getSwitch(3))
	{
		++(ctrl->LY);
	}

	if (SW.getSwitch(4))
	{
		--(ctrl->LY);
	}
	printf("(%d, %d)   ", ctrl->LY, ctrl->RY);
    vTaskDelay(100);
	return true;
}