#include "tank_tasks.hpp"
#include "stdio.h"
#include "stdbool.h"
#include <cstring>

bool MasterMeshNetReceive::init(void)
{
    #if DEBUG
        MasterMeshNetReceive::debugMode = true;
    #else
        MasterMeshNetReceive::debugMode = false;
    #endif

    return true;
}
bool MasterMeshNetReceive::run(void *p)
{
    vTaskDelay(50);    
	if (wireless_get_rx_pkt(&pkt, 10)) 
    {
        switch (pkt.nwk.src)
        {
            case TANK_MASTER :
                if (debugMode) printf("m;");
                memcpy(getSharedObject("TTM_Master"), pkt.data, sizeof(Tank_To_Master_t));
                break;
            case TANK_SLAVE :
                if (debugMode) printf("s;");
                memcpy(getSharedObject("TTM_Slave"), pkt.data, sizeof(Tank_To_Master_t));
                break;
            default :
                if (debugMode) printf("Couldn't decode this packet...\n");             
        }
    }
    else
    {
        if (debugMode) printf(".");
    }
    return true;
}