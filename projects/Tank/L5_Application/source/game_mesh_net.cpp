#include "user_defined_types.h"
#include "game_mesh_net.hpp"
#include "wireless.h"
#include "printf_lib.h"
#include <stdio.h>

Game_Mesh_Net::Game_Mesh_Net()
{
	ID = WIRELESS_NODE_ADDR;
}

void Game_Mesh_Net::NetID(uint8_t arg_ID)
{
	ID = arg_ID;
}

uint8_t Game_Mesh_Net::NetID() const
{
	return ID;
}

bool Game_Mesh_Net::ping(uint8_t arg_dest_addr, uint16_t arg_timeout_MS)
{
	mesh_packet_t ack_pkt;
    SemaphoreHandle_t   * xMutexSPI 	= (SemaphoreHandle_t *)scheduler_task::getSharedObject("xMutexSPI");

	if(xSemaphoreTake(*xMutexSPI, portMAX_DELAY) == pdTRUE)
    {
	    wireless_send(arg_dest_addr, mesh_pkt_ack, NULL, 0, MAX_HOPS);
	    if(wireless_get_ack_pkt(&ack_pkt, arg_timeout_MS))
	    {
	    	return true;
	    }
	    else
	    {
	    	return false;
	    }
        xSemaphoreGive(*xMutexSPI);
    }
}

bool Game_Mesh_Net::sendControllerToTank(uint16_t arg_timeout_MS)
{
	bool result;
	mesh_packet_t ack_pkt;
	uint8_t dest_addr;
    SemaphoreHandle_t   * xMutexSPI 	= (SemaphoreHandle_t *)scheduler_task::getSharedObject("xMutexSPI");
	switch(ID)
	{
		case MASTER :
			dest_addr = TANK_MASTER;
			break;
		case SLAVE :
			dest_addr = TANK_SLAVE;
			break;
		default :
			dest_addr = 0;
	}
	if (dest_addr)
	{
		Controller_t* CTT = (Controller_t*) scheduler_task::getSharedObject("Controller");

		wireless_send(dest_addr, mesh_pkt_ack, CTT, sizeof(Controller_t), MAX_HOPS);
		if (wireless_get_ack_pkt(&ack_pkt, arg_timeout_MS))
		{
			result = true;
		}
		else
		{
			result = false;
		}
	}
	else
	{
		result = false;
	}
	return result;
}

bool Game_Mesh_Net::sendTankToMaster(uint16_t arg_timeout_MS)
{
	bool result;
	mesh_packet_t ack_pkt;
	uint8_t dest_addr = MASTER;
	Tank_To_Master_t* TTM = (Tank_To_Master_t*) scheduler_task::getSharedObject("Tank_To_Master");

	wireless_send(dest_addr, mesh_pkt_ack, TTM, sizeof(Tank_To_Master_t), MAX_HOPS);

	if (wireless_get_ack_pkt(&ack_pkt, arg_timeout_MS))
	{
		result = true;
	}
	else
	{
		result = false;
	}
	#if DEBUG
		printf ("Master, this is %u\n", ID);
	#endif

	return result;
}

bool Game_Mesh_Net::sendMasterToTank(void* arg_MTT, uint8_t arg_dest_addr, uint16_t arg_timeout_MS)
{
	bool result;
	mesh_packet_t ack_pkt;
	SemaphoreHandle_t   * xMutexSPI 	= (SemaphoreHandle_t *)scheduler_task::getSharedObject("xMutexSPI");

	if (arg_dest_addr == TANK_SLAVE || arg_dest_addr == TANK_MASTER)
	{
		wireless_send(arg_dest_addr, mesh_pkt_ack, arg_MTT, sizeof(Master_To_Tank_t), MAX_HOPS);
		if (wireless_get_ack_pkt(&ack_pkt, arg_timeout_MS))
		{
			result = true;
		}
		else
		{
			result = false;
		}
	}
	else
	{
		result = false;
	}

	#if DEBUG
		printf ("Master To Tank %u\n", ID);
	#endif
	return result;
}