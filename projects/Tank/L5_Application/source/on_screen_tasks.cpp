#include "tank_tasks.hpp"

#include "scheduler_task.hpp"
#include "FreeRTOS.h"
#include "gpio.hpp"

#include "tlm/c_tlm_var.h"

void spi0_Init()
{
	LPC_SC->PCONP          |=  (1 << 21); // SPI1 Power Enable
	LPC_SC->PCLKSEL1       |=  (0b11 << 10); // CLK / 8

	// Select MISO, MOSI, and SCK pin-select functionality
	LPC_PINCON->PINSEL0    &= ~( (0b11 << 30) );
	LPC_PINCON->PINSEL1    &= ~( (0b11 <<  2) | (0b11 << 4) );
	LPC_PINCON->PINSEL0    |=  ( (0b10 << 30) );
	LPC_PINCON->PINSEL1    |=  ( (0b10 <<  2) | (0b10 << 4) );

	LPC_SSP0->CR0           =  7;         // 8-bit mode
	LPC_SSP0->CR1           =  (1 << 1);  // Enable SSP as Master
	LPC_SSP0->CPSR          =  60;         // SCK speed = CPU / 8
}

char spi0_ExchangeByte(char out)
{
	LPC_SSP0->DR = out;
	while(LPC_SSP0->SR & (1 << 4)); // Wait until SSP is busy
	return LPC_SSP0->DR;
}

void ChipSelect(bool Select)
{
	static GPIO ChipSelectPin(P1_19);
	static bool Initialized = false;
	if(!Initialized)
	{
		ChipSelectPin.setAsOutput();
		Initialized = true;
	}
	if(Select)
	{
		ChipSelectPin.setLow();
	}
	else
	{
		ChipSelectPin.setHigh();
	}
}

uint8_t SPITransfer(uint8_t Data)
{
	return spi0_ExchangeByte(Data);
}

bool OnScreenDisplayTask::init(void)
{
	spi0_Init();

	Controller_t * Controller = (Controller_t *)getSharedObject("Controller");
	Master_To_Tank_t * Master = (Master_To_Tank_t *)getSharedObject("Master_To_Tank");

    tlm_component * bucket = tlm_component_add("a");
    grouping = ((!Controller->BSTA) << 3 | Master->game_state);

    TLM_REG_VAR(bucket, Current_State, tlm_uint);
    TLM_REG_VAR(bucket, grouping, tlm_uint);

	Layout = new MaxLayoutManager(ChipSelect, SPITransfer);
	UserHP = new Bar();
	EnemyHP = new Bar();
	CrossHair = new Cursor();
	ScoreUI = new Score();
	PowerUpsUI = new PowerUps();

	Title = new Label("Lazer Tanks");
	StartGameMessage = new Label("Press Start to Begin Game");
	WaitingForOpponent = new Label("Waiting For Opponent");
	BeginMessage = new Label("::BEGIN::");
	GameOverTitle = new Label("GAME OVER");
	YouLose = new Label("A Loser Is You");
	YouWin = new Label("A Winner Is You");

	//// setCoords(x, y);

	Current_State = MAIN_MENU;

	UserHP->setLabel("HEALTH:");
	UserHP->set(100);
	UserHP->setCoords(1, 1);

	EnemyHP->setLabel("ENEMY:");
	EnemyHP->set(100);
	EnemyHP->setCoords(16, 1);

	ScoreUI->set(0);
	ScoreUI->setCoords(1, 11);

	PowerUpsUI->set(PowerUps::NONE);
	PowerUpsUI->setCoords(25, 11);

	Title->center();
	Title->setY(1);

	StartGameMessage->origin();

	WaitingForOpponent->origin();

	BeginMessage->origin();

	GameOverTitle->center();
	GameOverTitle->setY(2);

	YouLose->origin();
	YouWin->origin();

	Layout->Add(Title);
	Layout->Add(StartGameMessage);

	Layout->Initialize();

	Layout->Render();
	return true;
}
bool OnScreenDisplayTask::run(void *p)
{
	vTaskDelay(1000);

	Tank_To_Master_t 	* Self   		= (Tank_To_Master_t *)getSharedObject("Tank_To_Master");
	Master_To_Tank_t 	* Master 		= (Master_To_Tank_t *)getSharedObject("Master_To_Tank");
	Controller_t 		* Controller 	= (Controller_t *)getSharedObject("Controller");
	Game_Mesh_Net 		* Net 			= (Game_Mesh_Net *)getSharedObject("Net");

    SemaphoreHandle_t   * xMutexSPI 	= (SemaphoreHandle_t *)getSharedObject("xMutexSPI");
	uint8_t Start = !Controller->BSTA;

    grouping = (Current_State << 5 | Self->ready << 4 | Start << 3 | Master->game_state);

    // printf("hello");

	if(Current_State != Master->game_state)
	{
		Current_State = Master->game_state;
		// Current_State = FIGHTING;
		Layout->RemoveAll();
		Layout->ClearScreen();
		switch(Current_State)
		{
			case MAIN_MENU:
				Layout->Add(Title);
				Layout->Add(StartGameMessage);
				break;
			case WAITING:
				Layout->Add(WaitingForOpponent);
				break;
			case FIGHTING:
				Layout->Add(UserHP);
				Layout->Add(EnemyHP);
				Layout->Add(CrossHair);
				//Layout->Add(ScoreUI);
				Layout->Add(PowerUpsUI);
				break;
			case GAME_OVER_LOSE:
				Layout->Add(YouLose);
				Layout->Add(GameOverTitle);
				break;
			case GAME_OVER_WIN:
				Layout->Add(YouWin);
				Layout->Add(GameOverTitle);
				break;
		}
	}

	switch(Current_State)
	{
		case MAIN_MENU:
			if(Start)
			{
				Self->ready = true;
				//Net->sendTankToMaster(10);

			}
			break;
		case WAITING:
			if(Start)
			{
				Self->ready = true;
				//Net->sendTankToMaster(10);
			}
			break;
		case FIGHTING:
			UserHP->set(Self->health);
			EnemyHP->set(Master->opp_health);
			//ScoreUI->set(Self->Score);
			PowerUpsUI->set(Master->powerup_available);
			break;
		case GAME_OVER_LOSE:
			Self->ready = false;
			break;
		case GAME_OVER_WIN:
			Self->ready = false;
			break;
	}

	taskENTER_CRITICAL();
    Layout->Render();
	taskEXIT_CRITICAL();


	return true;
}