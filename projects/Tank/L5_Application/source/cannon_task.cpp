#include "tank_tasks.hpp"

#include "scheduler_task.hpp"
#include "FreeRTOS.h"
#include "gpio.hpp"

bool CannonTask::init(void)
{
	Cannon = new Servo(7);
	Cannon->initialize();
	Position = Servo::CENTER;
	return true;
}
bool CannonTask::run(void *p)
{
	Controller_t * Controller = (Controller_t *)getSharedObject("Controller");
	Master_To_Tank_t * Master = (Master_To_Tank_t *)getSharedObject("Master_To_Tank");

	uint8_t LT = !Controller->BLT;
	uint8_t RT = !Controller->BRT;

	// if(Master->game_state == FIGHTING)
	// {
		if(LT && RT)
		{
			Hold_Counter++;
			if(Hold_Counter >= 20)
			{
				Position = Servo::CENTER;
				Hold_Counter = 0;
			}
		}
		else if(LT)
		{
			Position += 20;
			Hold_Counter = 0;
		}
		else if(RT)
		{
			Position -= 20;
			Hold_Counter = 0;
		}
		Position = Cannon->setPulse(Position);
	// }
	vTaskDelay(20);

	return true;
}