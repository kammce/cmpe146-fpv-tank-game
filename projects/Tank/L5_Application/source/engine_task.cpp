#include "tank_tasks.hpp"
#include "MaxElements.hpp"
#include <stdlib.h>

#include "utilities.h"
#include "printf_lib.h"
#include "io.hpp"
using namespace std;

bool EngineTask::init (void)
{
	// MTT_Master.powerup_available = PowerUps::POWER_UPS_CODE_E(0);

	// MTT_Slave.powerup_available = PowerUps::POWER_UPS_CODE_E(0);

	MTT_Master = (Master_To_Tank_t *)getSharedObject("MTT_Master");
	MTT_Master->opp_health = 5;
	MTT_Master->game_state = MAIN_MENU;

	MTT_Slave = (Master_To_Tank_t *)getSharedObject("MTT_Slave");
	MTT_Slave->opp_health = 5;
	MTT_Slave->game_state = MAIN_MENU;

	TTM_Master = (Tank_To_Master_t *)getSharedObject("TTM_Master");
	TTM_Slave = (Tank_To_Master_t *)getSharedObject("TTM_Slave");

	Net = (Game_Mesh_Net *)getSharedObject("Net");

	return true;
}

bool EngineTask::run (void * p)
{
	vTaskDelay (50);
	Net->sendMasterToTank (MTT_Master, 202, 10);
	Net->sendMasterToTank (MTT_Slave, 203, 10);

	// if (Net->sendMasterToTank (202, 100))
	// 	printf ("Tank# 1 sent an Ack \n");
	// else
	// 	printf ("Tank# 1 sent an Nack \n");

	// if (Net->sendMasterToTank (203, 100))
	// 	printf ("Tank# 2 sent an Ack \n");
	// else
	// 	printf ("Tank# 2 sent an Nack \n");

	MTT_Master->opp_health = TTM_Slave->health;
	MTT_Slave->opp_health = TTM_Master->health;

	switch (MTT_Master->game_state)
	{
		case MAIN_MENU:
			if (TTM_Master->ready) MTT_Master->game_state = WAITING;

			#if DEBUG
				printf ("M - Main Menu \n");
			#endif
		break;

		case WAITING:
			if (TTM_Slave->ready) MTT_Master->game_state = FIGHTING;

			#if DEBUG
				printf ("M - Waiting \n");
			#endif
		break;

		case FIGHTING:

			if      (!TTM_Master->health)     MTT_Master->game_state = GAME_OVER_LOSE;
			else if (!MTT_Master->opp_health) MTT_Master->game_state = GAME_OVER_WIN;

			#if DEBUG
				printf ("M - Fighting \n");
			#endif
		break;

		case GAME_OVER_LOSE:

			#if DEBUG
				printf ("M - Get Rekt Nerd \n");
			#endif
		break;

		case GAME_OVER_WIN:

			#if DEBUG
				printf ("M - You Have Some Skill \n");
			#endif
		break;
	}



	switch (MTT_Slave->game_state)
	{
		case MAIN_MENU:
			if (TTM_Slave->ready) MTT_Slave->game_state = WAITING;

			#if DEBUG
				printf ("S - Main Menu \n");
			#endif
		break;

		case WAITING:
			if (TTM_Master->ready) MTT_Slave->game_state = FIGHTING;

			#if DEBUG
				printf ("S  - Waiting \n");
			#endif
		break;

		case FIGHTING:
			if      (!TTM_Slave->health)     MTT_Slave->game_state = GAME_OVER_LOSE;
			else if (!MTT_Slave->opp_health) MTT_Slave->game_state = GAME_OVER_WIN;

			#if DEBUG
				printf ("M - Fighting \n");
			#endif
		break;

		case GAME_OVER_LOSE:

			#if DEBUG
				printf ("S - Get Rekt Nerd \n");
			#endif
		break;

		case GAME_OVER_WIN:

			#if DEBUG
				printf ("S - You Have Some Skill \n");
			#endif
		break;
	}
	return true;
}

				// vTaskDelay(2000);
				// if (!MTT_Slave.powerup_available) MTT_Slave.powerup_available = PowerUps::POWER_UPS_CODE_E(rand() % 4 + 1);

				// if (SW.getSwitch(3))
				// {
				// 	TTM_Master.health--;
				// 	MTT_Slave.opp_health--;
				// 	u0_dbg_printf ("HP : %i \n", TTM_Master.health);

				// 	TTM_Master.powerup_used = MTT_Master.powerup_available;
				// 	u0_dbg_printf ("M - power used %i \n", TTM_Master.powerup_used);
				// 	MTT_Master.powerup_available = PowerUps::POWER_UPS_CODE_E(0);
				// }