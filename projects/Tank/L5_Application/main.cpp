#include <stdio.h>
#include "tasks.hpp"
#include "examples/examples.hpp"
#include "periodic_scheduler/periodic_callback.h"
#include "uart2.hpp"
#include "uart3.hpp"
#include "utilities.h"
#include "tank_tasks.hpp"
#include "game_mesh_net.hpp"
#include <string.h>

Controller_t Controller;
Game_Mesh_Net Net;


Master_To_Tank_t Master_To_Tank;
Tank_To_Master_t Tank_To_Master;


Master_To_Tank_t MTT_Master;
Master_To_Tank_t MTT_Slave;
Tank_To_Master_t TTM_Master;
Tank_To_Master_t TTM_Slave;

SemaphoreHandle_t xSemTerminal;
SemaphoreHandle_t xMutexSPI;

int main(void)
{
    xMutexSPI = xSemaphoreCreateMutex();

    memset(&Controller, 0xFF, sizeof(Controller_t));
    Controller.packetID = Packet_Src_t::CONTROLLER_ID;
    Controller.LY = 32;
    Controller.RY = 16;

    Master_To_Tank.game_state = MAIN_MENU;

    scheduler_add_task(new terminalTask(PRIORITY_CRITICAL));
    scheduler_add_task(new wirelessTask(PRIORITY_CRITICAL));
    // const bool run_1Khz = false;
    //scheduler_add_task(new periodicSchedulerTask(run_1Khz));

    scheduler_task::addSharedObject("xMutexSPI", &xMutexSPI);

    #if ROLE != MASTER
        scheduler_task::addSharedObject("Master_To_Tank", &Master_To_Tank);
        scheduler_task::addSharedObject("Tank_To_Master", &Tank_To_Master);
    #endif

    scheduler_task::addSharedObject("Controller", &Controller);
    scheduler_task::addSharedObject("Net", &Net);

    #if ROLE == MASTER
        scheduler_task::addSharedObject("MTT_Master", &MTT_Master);
        scheduler_task::addSharedObject("MTT_Slave", &MTT_Slave);
        scheduler_task::addSharedObject("TTM_Master", &TTM_Master);
        scheduler_task::addSharedObject("TTM_Slave", &TTM_Slave);
    #endif

    #if DEBUG
        printf("RUNNING IN TANK DEBUG MODE!!!\n");

        // uint8_t R_input;
        // uint8_t L_input;
        // xSemTerminal = xSemaphoreCreateBinary();
        // scheduler_task::addSharedObject("R_input", &R_input);
        // scheduler_task::addSharedObject("L_input", &L_input);
        // scheduler_task::addSharedObject("xSemTerminal", &xSemTerminal);

        // scheduler_add_task(new EngineTaskTest(PRIORITY_MEDIUM));
        scheduler_add_task(new DriveSystemTest(PRIORITY_MEDIUM));

    #endif

    #if ROLE == SLAVE
        printf("SLAVE ROLE :: %d :: %d\n", ROLE, WIRELESS_NODE_ADDR);
    #elif ROLE == MASTER
        printf("MASTER ROLE :: %d :: %d\n", ROLE, WIRELESS_NODE_ADDR);
    #elif ROLE == TANK_SLAVE
        printf("SLAVE TANK :: %d :: %d\n", ROLE, WIRELESS_NODE_ADDR);
    #elif ROLE == TANK_MASTER
        printf("MASTER TANK :: %d :: %d\n", ROLE, WIRELESS_NODE_ADDR);
    #else
        printf("NO ROLE %d :: %d\n", ROLE, WIRELESS_NODE_ADDR);
    #endif

    #if ROLE == SLAVE || ROLE == MASTER
        scheduler_add_task(new ControllerTask(PRIORITY_HIGH));
    #endif
    #if ROLE == MASTER
        scheduler_add_task(new EngineTask(PRIORITY_MEDIUM));
        scheduler_add_task(new MasterMeshNetReceive(PRIORITY_HIGH));
        // Add appropriate shared objects
    #endif
    #if ROLE == TANK_SLAVE || ROLE == TANK_MASTER
        //scheduler_add_task(new OnScreenDisplayTask(PRIORITY_LOW));
        scheduler_add_task(new MeshNetReceive(PRIORITY_LOW));
        scheduler_add_task(new DriveSystem(PRIORITY_MEDIUM));
        scheduler_add_task(new CannonTask(PRIORITY_MEDIUM));
        scheduler_add_task(new FireTask(PRIORITY_MEDIUM));
        scheduler_add_task(new LaserTask(PRIORITY_MEDIUM));
        //scheduler_add_task(new HitTask(PRIORITY_MEDIUM));
    #endif

    scheduler_start(); ///< This shouldn't return
    return -1;
}