#ifndef GAME_MESH_NET_H
#define GAME_MESH_NET_H

#include "stdbool.h"
#include "wireless.h"
#include "tasks.hpp"
#include "user_defined_types.h"

#define MAX_HOPS 4

class Game_Mesh_Net
{
public:
	Game_Mesh_Net();

	// Functionally-unneeded setter. Available for testing/debugging.
	void NetID(uint8_t);
	uint8_t NetID() const;

	// Returns Nack (False) or Ack (True)
	// Destination address is deduced based on source address
	//   except in Master to Tank communications
	// Timeout (in MS) argument is always required (uint16_t)
	bool sendControllerToTank(uint16_t);
	bool sendTankToMaster(uint16_t);
	bool sendMasterToTank(void*, uint8_t, uint16_t);
	bool ping(uint8_t, uint16_t);

private:
	uint8_t ID;
};

#endif