#ifndef USER_DEFINED_TYPES_H
#define USER_DEFINED_TYPES_H
#include "MaxElements.hpp"

/* from the makefile
---------------------
    -D MASTER=200 \
    -D SLAVE=201 \
    -D TANK_MASTER=202 \
    -D TANK_SLAVE=203 \

*/
enum GameState_e
{
    MAIN_MENU = 0,
    WAITING,
    FIGHTING,
    GAME_OVER_LOSE,
    GAME_OVER_WIN,
};

// typedef enum 
// {
//     NORTH = 0,
//     EAST,
//     SOUTH,
//     WEST,
// } Side_t;

// typedef enum
// {
//     shared_SensorQueueId,
// }sharedHandleId_t;

typedef enum
{
    // packet enumeration = packet size
    TANK_ID = 2,
    MASTER_ID = 3,
    CONTROLLER_ID = 7
} Packet_Src_t;

// typedef struct
// {
//     uint8_t north : 1, east : 1, south : 1, west : 1; //not const to make memcpy easy on receiving end
// } Hit_Status_t;

typedef struct
{
    uint8_t packetID = Packet_Src_t::TANK_ID; //not const to make memcpy easy on receiving end
    unsigned int ready : 1;
    unsigned int health : 3;
    unsigned int hit : 1;
    PowerUps::POWER_UPS_CODE_E powerup_used : 3;
} Tank_To_Master_t;

typedef struct
{
    uint8_t packetID = Packet_Src_t::MASTER_ID; //not const to make memcpy easy on receiving end
    unsigned int opp_health : 3;
    PowerUps::POWER_UPS_CODE_E powerup_available : 3;
    GameState_e game_state : 3;
    unsigned int padding_bits : 7;  //padding for mesh payload
} Master_To_Tank_t;

typedef struct
{
    uint8_t packetID = Packet_Src_t::CONTROLLER_ID; //not const to make memcpy easy on receiving end
    uint8_t LX : 6, RX43 : 2;
    uint8_t LY : 6, RX21 : 2;
    uint8_t RY : 5, LT43 : 2, RX0 : 1;
    uint8_t RT : 5, LT20 : 3;
    uint8_t :1, BRT : 1, BSTA : 1, BHOM : 1, BSEL : 1, BLT : 1, BDD : 1, BDR : 1;
    uint8_t BDU : 1, BDL : 1, BZR : 1, BX : 1, BA : 1, BY : 1, BB : 1, BZL : 1;

} Controller_t;

#endif