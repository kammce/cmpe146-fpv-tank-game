#ifndef WIIPROCONTROLLER_H
#define WIIPROCONTROLLER_H

#include "user_defined_types.h"

enum class Stick
{
    Left,
    Right
};

enum class Shoulder
{
    Left,
    Right
};

enum class Button 
{
    A,
    B,
    X,
    Y,
    Up,
    Down,
    Left,
    Right,
    Home,
    Start,
    Select,
    L,
    R,
    ZL,
    ZR
};

class WiiProController
{
private:
	// 6-byte raw data from controller
	union
	{
		uint8_t raw[7];
		Controller_t ctrl;
	};
public:
	WiiProController();
	bool Initialize();
	bool IsPressed(Button button);
	void Print();
	void Update();
	uint8_t Pressure(Shoulder shoulder);
	uint8_t StickX(Stick stick);
	uint8_t StickY(Stick stick);
	Controller_t Read();
	void Set(Controller_t);
};


#endif