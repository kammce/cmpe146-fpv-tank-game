//tank_motor.hpp

/*
	converts speed & direction to H-Bridge PWM control signals
	// from lpc_pwm.hpp
	typedef enum {
	    pwm1=0, ///< P2.0
	    pwm2=1, ///< P2.1
	    pwm3=2, ///< P2.2
	    pwm4=3, ///< P2.3
	    pwm5=4, ///< P2.4
	    pwm6=5  ///< P2.5
	} pwmType;
*/	
#ifndef TANKMOTOR_H
#define TANKMOTOR_H


#include "stdint.h"
#include "lpc_pwm.hpp"


class TankMotor 
{
public:
	typedef enum 
	{
		Backward = 0,
		Forward = 1
	} Motor_Direction_E;

	static constexpr double DEFAULT_POWER_VOLTAGE = 9.0f;
	static constexpr double DEFAULT_RATED_VOLTAGE = 3.0f;
	static const unsigned int PWM_FREQUENCY = 38000;
	static const uint8_t MAX_PWM = 100;	//SJOne Board API takes percentage input
	static const uint8_t MIN_PWM = 0;	//SJOne Board API takes percentage input
	
	TankMotor(PWM::pwmType, PWM::pwmType);
	TankMotor(double, double, PWM::pwmType, PWM::pwmType);
	TankMotor(TankMotor&);
	
	double PowerVoltage() const;
	double RatedVoltage() const;
	uint8_t MotorSpeed() const;
	Motor_Direction_E MotorDir() const;

	void PowerVoltage(double);
	void RatedVoltage(double);
	void MotorSpeed(uint8_t);
	void MotorDir(Motor_Direction_E);
private:
	double Power_Voltage;	//9V default 
	double Rated_Voltage;	//3V default
	PWM Motor_IN1;			//PWM API (Social ledge)
	PWM Motor_IN2;			//PWM API (Social ledge)
	PWM::pwmType Motor_IN1_Port;	//DRV8871 IN1 terminal	
	PWM::pwmType Motor_IN2_Port;	//DRV8871 IN2 terminal
	uint8_t Motor_Speed;	//0% to 100% duty cycle
	Motor_Direction_E Motor_Dir;

	void MapToHBridge();
};

#endif // TANKMOTOR_H