// tank_motor.cpp
#include "tank_motor.hpp"


void TankMotor::MapToHBridge()
{
	// Motor_Speed_Adj: adjusting DRV8871 PWM inputs to the power/rated voltages
	uint8_t Motor_Speed_Adj = Motor_Speed * (Rated_Voltage / Power_Voltage);
    Motor_Speed_Adj = Motor_Speed_Adj > MAX_PWM? MAX_PWM : Motor_Speed_Adj;
    Motor_Speed_Adj = Motor_Speed_Adj < MIN_PWM? MIN_PWM : Motor_Speed_Adj;
    Motor_IN1.set(Motor_Dir? MAX_PWM - Motor_Speed_Adj : MAX_PWM);
    Motor_IN2.set(Motor_Dir? MAX_PWM : MAX_PWM - Motor_Speed_Adj);
}

TankMotor::TankMotor(PWM::pwmType Usr_IN1_Port, PWM::pwmType Usr_IN2_Port) 
: 	Motor_IN1(Usr_IN1_Port, PWM_FREQUENCY), 
	Motor_IN2(Usr_IN2_Port, PWM_FREQUENCY)
{
	Power_Voltage = DEFAULT_POWER_VOLTAGE;
	Rated_Voltage = DEFAULT_RATED_VOLTAGE;
	Motor_Speed = 0;
	Motor_Dir = Forward;
	MapToHBridge();
}

TankMotor::TankMotor(double Usr_Power_Voltage, double Usr_Rated_Voltage, PWM::pwmType Usr_IN1_Port, PWM::pwmType Usr_IN2_Port)
: 	Motor_IN1(Usr_IN1_Port, PWM_FREQUENCY), 
	Motor_IN2(Usr_IN2_Port, PWM_FREQUENCY)
{
	Power_Voltage = Usr_Power_Voltage;
	Rated_Voltage = Usr_Rated_Voltage;
	Motor_Speed = 0;
	Motor_Dir = Forward;
	MapToHBridge();
}

TankMotor::TankMotor(TankMotor& Usr_Tank_Motor)
: 	Motor_IN1(Usr_Tank_Motor.Motor_IN1_Port, PWM_FREQUENCY), 
	Motor_IN2(Usr_Tank_Motor.Motor_IN2_Port, PWM_FREQUENCY)
{
	Power_Voltage = Usr_Tank_Motor.Power_Voltage;
	Rated_Voltage = Usr_Tank_Motor.Rated_Voltage;
	Motor_Speed = Usr_Tank_Motor.Motor_Speed;
	Motor_Dir = Usr_Tank_Motor.Motor_Dir;
	MapToHBridge();
}

double TankMotor::PowerVoltage() const
{
	return Power_Voltage;
}

double TankMotor::RatedVoltage() const
{
	return Rated_Voltage;
}

uint8_t TankMotor::MotorSpeed() const
{
	return Motor_Speed;
}

TankMotor::Motor_Direction_E TankMotor::MotorDir() const
{
	return Motor_Dir;
}

void TankMotor::PowerVoltage(double Usr_Power_Voltage)
{
	Power_Voltage = Usr_Power_Voltage;
	MapToHBridge();
}
void TankMotor::RatedVoltage(double Usr_Rated_Voltage)
{
	Rated_Voltage = Usr_Rated_Voltage;
	MapToHBridge();
}
void TankMotor::MotorSpeed(uint8_t Usr_Speed)
{
	Motor_Speed = Usr_Speed;
	MapToHBridge();
}
void TankMotor::MotorDir(Motor_Direction_E Usr_Dir)
{
	Motor_Dir = Usr_Dir;
	MapToHBridge();
}