
// // tank_drive_system.cpp


// #include "tank_drive_system.hpp"

// TankDriveSystem::Drive_System_State_S TankDriveSystem::UpdateState()
// {
// 	Tank_State.RM_Speed = RM.MotorSpeed();
// 	Tank_State.LM_Speed = LM.MotorSpeed();
// 	Tank_State.RM_Dir = RM.MotorDir();
// 	Tank_State.LM_Dir = LM.MotorDir();

// 	return Tank_State;
// }

// TankDriveSystem::TankDriveSystem(PWM::pwmType RM1, PWM::pwmType RM2, PWM::pwmType LM1, PWM::pwmType LM2)
// :	RM(RM1, RM2),
// 	LM(LM1, LM2)
// {
// 	UpdateState();
// }

// TankDriveSystem::Drive_System_State_S TankDriveSystem::State()
// {
// 	return UpdateState();
// }

// TankDriveSystem::Drive_System_State_S TankDriveSystem::Move(uint8_t Usr_Speed, int8_t Usr_Steering_Offset, 
// 															TankMotor::Motor_Direction_E Usr_Dir)
// {
// 	Usr_Steering_Offset = Usr_Steering_Offset < STEERING_NEGATIVE_LIMIT ? STEERING_NEGATIVE_LIMIT : Usr_Steering_Offset;
// 	Usr_Steering_Offset = Usr_Steering_Offset > STEERING_POSITIVE_LIMIT ? STEERING_POSITIVE_LIMIT : Usr_Steering_Offset;
// 	uint8_t LM_Usr_Speed_Factor = Usr_Steering_Offset < 0 ? TankMotor::MAX_PWM + Usr_Steering_Offset : TankMotor::MAX_PWM;
// 	uint8_t RM_Usr_Speed_Factor = Usr_Steering_Offset > 0 ? TankMotor::MAX_PWM - Usr_Steering_Offset : TankMotor::MAX_PWM;
// 	LM.MotorSpeed(Usr_Speed * (double(LM_Usr_Speed_Factor) / 100.0)); 
// 	RM.MotorSpeed(Usr_Speed * (double(RM_Usr_Speed_Factor) / 100.0));
// 	LM.MotorDir(Usr_Dir);
// 	RM.MotorDir(Usr_Dir);

// 	return UpdateState();
// }


// TankDriveSystem::Drive_System_State_S TankDriveSystem::Spin(uint8_t Usr_Speed, Tank_Spin_E Usr_Dir)
// {
// 	TankMotor::Motor_Direction_E Usr_RM_Dir = Clockwise == Usr_Dir ? 
// 											  TankMotor::Motor_Direction_E::Backward : TankMotor::Motor_Direction_E::Forward;
// 	TankMotor::Motor_Direction_E Usr_LM_Dir = TankMotor::Motor_Direction_E::Backward == Usr_RM_Dir ? 
// 											  TankMotor::Motor_Direction_E::Forward : TankMotor::Motor_Direction_E::Backward;
// 	LM.MotorSpeed(Usr_Speed);
// 	LM.MotorDir(Usr_LM_Dir);
// 	RM.MotorSpeed(Usr_Speed);
// 	RM.MotorDir(Usr_RM_Dir);
// 	return UpdateState();
// }

// TankDriveSystem::Drive_System_State_S TankDriveSystem::Stop(void)
// {
// 	LM.MotorSpeed(TankMotor::MIN_PWM);
// 	RM.MotorSpeed(TankMotor::MIN_PWM);
// 	return UpdateState();

// }
