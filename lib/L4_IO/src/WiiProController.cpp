#include <stdio.h>
#include <string.h>
#include "WiiProController.hpp"
#include "i2c2.hpp"


WiiProController::WiiProController()
{
    ctrl.packetID = Packet_Src_t::CONTROLLER_ID;
}

bool WiiProController::Initialize()
{
    if (!I2C2::getInstance().checkDeviceResponse(0xa4)) 
    {
        return 1;
    }
    else
    {   
        // Initialize with encryption xor key 0x00
        I2C2::getInstance().writeReg(0xa4, 0x40, 0x00);
    }
    return 0;
}

void WiiProController::Update()
{
    I2C2::getInstance().readRegisters(0xa4, 0x00, &raw[1], 6);
    // Reset internal register to 0
    I2C2::getInstance().writeReg(0xa4, 0x00, 0);
}

void WiiProController::Print()
{
    Update();
    printf("%02x%02x%02x%02x%02x%02x\n", raw[1], raw[2], raw[3], raw[4], raw[5], raw[6]);
}  

Controller_t WiiProController::Read()
{
    Update();
    return ctrl;
}

void WiiProController::Set(Controller_t CTL)
{
    ctrl = CTL;
}

bool WiiProController::IsPressed(Button button)
{
    Update();
    switch(button)
    {
        case Button::Up:    return (!ctrl.BDU);
        case Button::Left:  return (!ctrl.BDL);
        case Button::ZR:    return (!ctrl.BZR);
        case Button::X:     return (!ctrl.BX);
        case Button::A:     return (!ctrl.BA);
        case Button::Y:     return (!ctrl.BY);
        case Button::B:     return (!ctrl.BB);
        case Button::ZL:    return (!ctrl.BZL);
        case Button::R:     return (!ctrl.BRT);
        case Button::Start: return (!ctrl.BSTA);
        case Button::Home:  return (!ctrl.BHOM);
        case Button::Select:return (!ctrl.BSEL);
        case Button::L:     return (!ctrl.BLT);
        case Button::Down:  return (!ctrl.BDD);
        case Button::Right: return (!ctrl.BDR);
    }
    return 0;
}

uint8_t WiiProController::Pressure(Shoulder shoulder) // Knock-off controller has no pressure?
{
    Update();
    switch(shoulder)
    {
        // Should refactor to use structure instead of raw data
        case Shoulder::Left: return ((raw[3+1] & 0x1F) >> 5) | ((raw[2+1] & 0x6) >> 3);
        case Shoulder::Right: return (raw[3+1] & 0x1F); 
    }
    return 0;
}

uint8_t WiiProController::StickX(Stick stick)
{
    Update();
    switch(stick)
    {
        // Should refactor to use structure instead of raw data
        case Stick::Left: return (raw[0+1] & 0x3F);
        case Stick::Right: return (((raw[0+1] & 0xC0) >> 3) | ((raw[1+1] & 0xC0) >> 5) | ((raw[2+1] & 0x80) >> 7));
    }
    return 0;
}

uint8_t WiiProController::StickY(Stick stick)
{
    Update();
    switch(stick)
    {
        case Stick::Left: return ctrl.LY;
        case Stick::Right: return ctrl.RY;
    }
    return 0;
}