
// // tank_drive_system.hpp

// #ifndef TANKDRIVESYSTEM_H
// #define TANKDRIVESYSTEM_H

// #include "stdint.h"
// #include "lpc_pwm.hpp"
// #include "tank_motor.hpp"




// class TankDriveSystem 
// {
// public:
// 	static const int8_t STEERING_POSITIVE_LIMIT = 100;
// 	static const int8_t STEERING_NEGATIVE_LIMIT = -STEERING_POSITIVE_LIMIT;

// 	typedef enum 
// 	{
// 		Counter_Clockwise = 0,
// 		Clockwise = 1
// 	} Tank_Spin_E;
	
// 	struct Drive_System_State_S
// 	{
// 		uint8_t RM_Speed;
// 		uint8_t LM_Speed;
// 		TankMotor::Motor_Direction_E RM_Dir;
// 		TankMotor::Motor_Direction_E LM_Dir;
// 	};

// 	TankDriveSystem(PWM::pwmType, PWM::pwmType, PWM::pwmType, PWM::pwmType);

// 	Drive_System_State_S State();

// 	Drive_System_State_S Move(uint8_t, int8_t, TankMotor::Motor_Direction_E);
// 	Drive_System_State_S Spin(uint8_t, Tank_Spin_E);
// 	Drive_System_State_S Stop(void);
// // possible feature	void Impact(Side_E);

// private:
// 	TankMotor RM; //Right_Motor
// 	TankMotor LM; //Left_Motor
// 	Drive_System_State_S Tank_State;

// 	Drive_System_State_S UpdateState();
// };

// #endif // TANKDRIVESYSTEM_H
