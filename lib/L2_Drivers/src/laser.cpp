#include "laser.hpp"

laser :: laser (int myPort, int myPin)
{
	port = myPort;
	pin = myPin;
}

void laser :: activatePin ()
{
	if (port == 0)
	{
	    LPC_GPIO0 -> FIODIR |= (1 << pin);
    	LPC_GPIO0 -> FIOSET  = (1 << pin);
	}
	else if (port == 1)
	{
		LPC_GPIO1 -> FIODIR |= (1 << pin);
		LPC_GPIO1 -> FIOSET  = (1 << pin);
	}
	else if (port == 2)
	{
	    LPC_GPIO2 -> FIODIR |= (1 << pin);
		LPC_GPIO2 -> FIOSET  = (1 << pin);
	}
}

void laser :: deactivatePin ()
{
	if (port == 0)
	{
    	LPC_GPIO0 -> FIOCLR  = (1 << pin);
	}
	else if (port == 1)
	{
		LPC_GPIO1 -> FIOCLR  = (1 << pin);
	}
	else if (port == 2)
	{
		LPC_GPIO2 -> FIOCLR  = (1 << pin);
	}
}