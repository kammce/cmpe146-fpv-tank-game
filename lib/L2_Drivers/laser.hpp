#ifndef laserhpp_
#define laserhpp_

#include "LPC17xx.h"

class laser
{
	private:
		int port;
		int pin;

	public:
		laser (int myPort, int myPin);
		void activatePin ();
		void deactivatePin();
};

#endif