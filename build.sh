#!/bin/bash

if [ "$#" -lt 1 ]
then
  echo -e "Usage: ./build.sh <name of project>"
  exit 1
fi

# Setup Project Target
PROJECT_NAME=$1
ENTITY_NAME=DBG
ROLE=$2

if [ "$3" == "DEBUG" ]
then
	make PROJ=$PROJECT_NAME ENTITY=$ENTITY_NAME ROLE=$ROLE -j8 DEBUG=1
else
	make PROJ=$PROJECT_NAME ENTITY=$ENTITY_NAME ROLE=$ROLE -j8
fi

